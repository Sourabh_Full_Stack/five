const express = require("express");
const router = express.Router();
const Users = require("../Models/User");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

router.get("/", (req, res, next) => {
  res.status(200).json({
    message: "This is from router.",
  });
});

router.post("/signup", (req, res, next) => {
  Users.find({ email: req.body.email })
    .then((result) => {
      if (result.length >= 1) {
        res.status(409).json({
          message:
            "an account with this email id already exists, try signing up with a new email.",
        });
      } else {
        bcrypt.hash(req.body.password, 10, (error, hash) => {
          if (error) {
            res.status(500).json({
              message: "something went wrong, try again.",
            });
          } else {
              req.body.password = hash
            const user = new Users(req.body);
            user
              .save()
              .then((result) => {
                res.status(201).json({
                  message: "User saved successfully",
                  serverResponse: result,
                });
              })
              .catch((error) => [
                res.status(500).json({
                  message: "something went wrong try again.!",
                }),
              ]);   
          }
        });
      }
    })
    .catch((error) => {
      console.log(error);
      res.status(500).json({
        message: "something went wrong try again.!!",
      });
    });
});

router.post("/login", (req, res, next) => {
  Users.find({ email: req.body.email })
    .then((result) => {
      if (result.length < 1) {
        res.status(404).json({
          message:
            "no account found with this email id, try signing with a registered mail id.",
        });
      } else {
        bcrypt.compare(
          req.body.password,
          result[0].password,
          (error, response) => {
            if (error) {
              res.status(500).json({
                message: "something went wrong, try again.",
              });
            }
            if (response) {
              jwt.sign(
                {
                  Email: req.body.email,
                },
                "sr3crt4",
                {
                  expiresIn: "1h",
                },
                (error, token) => {
                  if (error) {
                    res.status(500).json({
                      message: "sometheing went wrong, try again.",
                    });
                  }
                  if (token) {
                    res.status(200).json({
                      message: "User successfully logged in.",
                      AuthToken: token,
                    });
                  }
                }
              );
            }else{
                res.status(401).json({
                    message: 'Auth failed.'
                })
            }
          }
        );
      }
    })
    .catch((error) => {
      res.status(500).json({
        message: "something went wrong, try again.",
      });
    });
});

router.get('/all',(req,res,next)=>{
  Users.find()
  .then(result=>{
    if(result.length < 1) {
      res.status(404).json({
        message: 'No data found'
      })
    }else{
      res.status(200).json({
        message: 'All users list : ',
        serverResponse : result
      })
    }
  })
  .catch(error=>{
    res.status(500).json({
      message: 'something went wrong.',
      errorOccured: error
    })
  });
})

module.exports = router;
