const express = require('express');
const app = express();
const userRouter = require('./src/API/Routes/Users');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const cors = require('cors');

app.listen(3001, () =>{
    console.log('Server is running');
});

app.use(cors());

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use('/user',userRouter);

mongoose.connect('mongodb+srv://SourabhFullStack:Hichki1990@cluster0-4czp9.mongodb.net/london?retryWrites=true&w=majority',{ useNewUrlParser: true ,useUnifiedTopology: true })
.then(result=>{
    if(result){
        console.log('Database connected successfully.');
    }
})
.catch(error=>{
    if(error){
        console.log('database not connected.')
    }
});


app.get('/welcome',(req,res,next)=>{
    res.status(200).json({
        message: 'This is working all good.'
    })
})

module.exports = app